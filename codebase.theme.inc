<?php
/**
 * @file
 *  Theme functions for Codebase module.
 */

/**
 * Render the codebase information.
 */
function theme_codebase_info($parameters = array()) {
  $items = array();
  if (codebase_git_is_active()) {
    $items[] = theme('codebase_item', theme('codebase_commit', $parameters), t('Commit'));
    $items[] = theme('codebase_item', $parameters['date'], t('Date'));
    $items[] = theme('codebase_item', theme('codebase_branch', $parameters), t('Branch'));
    $items[] = theme('codebase_item', theme('codebase_last_tag', $parameters));
  }
  if (module_exists('environment')) {
    $items[] = theme('codebase_item', $parameters['environment'], t('Environment'));
  }
  if (empty($items)) { 
    return NULL;
  }
  return theme('item_list', $items);
}

/**
 * Render an individual label/value pair.
 */
function theme_codebase_item($value, $label = NULL) {
  $label = !empty($label) ? '<strong>' . $label . '</strong>: ' : '';
  return $label . $value;
}

/**
 * Render the code version id.
 */
function theme_codebase_version($parameters = array(), $delim = '; ') {
  return theme('codebase_branch', $parameters, array('plain' => TRUE))
    . $delim
    . theme('codebase_commit', $parameters, array('plain' => TRUE));
}

/**
 * Render a git commit.
 */
function theme_codebase_commit($parameters = array(), $options = array()) {
  $title = t('Author:') . " {$parameters['author']} - {$parameters['message']}";  
  if (isset($parameters['commit_url']) && empty($options['plain'])) {
    $tag = 'a href="' . $parameters['commit_url'] . '"';
    $endtag = 'a';
  }
  else {
    $tag = $endtag = 'span';
  }
  return '<' . $tag . ' title="' . $title . '" class="codebase-commit">' . $parameters['commit_id'] . "</$endtag>";
}

/**
 * Render a git branch.
 */
function theme_codebase_branch($parameters = array(), $options = array()) {
  if (isset($parameters['branch_url']) && empty($options['plain'])) {
    $tag = 'a href="' . $parameters['branch_url'] . '"';
    $endtag = 'a';
  }
  else {
    $tag = $endtag = 'span';
  }
  return '<' . $tag . ' class="codebase-commit">' . $parameters['branch'] . "</$endtag>";
}

/**
 * Render the most recent tag.
 */
function theme_codebase_last_tag($parameters = array()) {
  $prefix = '';
  if (!empty($parameters['commits_since'])) {
    $prefix = format_plural($parameters['commits_since'], '1 commit', '@count commits since') . ' ';
  }
  return $prefix . '<em>' . $parameters['tag'] . '</em>';
}

/**
 * Render the data for Taskbar integration.
 */
function theme_codebase_taskbar_item($parameters = array()) {
  $output = '<div id="codebase_taskbar" style="background-position:8px -918px;">' . theme('codebase_version', $parameters, '<br />') . '</div>';
  return $output;
}

/**
 * Render the longform description of the last commit.
 */
function theme_codebase_commit_description($parameters = array()) {
  $text = $parameters['author'] . ' on ' . $parameters['date'] . ': ' . $parameters['message'];
  if (isset($parameters['path'])) {
    $text = '<a href="' . $path . '">' . $text . '</a>';
  }
  return $text;
}
