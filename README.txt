Codebase module
---------------

Provides some codebase and deployment information as a list-oriented block.
Separation and static caching of info combined with theme functions allows 
easy developer integration of this information however they like in the UI.

Pulls Git information, optionally Environment module information.

Has Taskbar integration built in to expose this information to appropriate users.

admin/reports/status also displays the current branch and latest commit.

