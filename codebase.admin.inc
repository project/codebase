<?php
/**
 * @file
 * Codebase module admin pages.
 */

/**
 * Admin settings callback.
 */
function codebase_admin_settings(&$form_state) {
  $form = array();

  if (codebase_git_is_active()) {
    $form['repo'] = array(
      '#type' => 'fieldset',
      '#title' => t('Repository settings'),
      '#desription' => t('Settings related to code repository. Filling in these values will facilitate links.'),
    );
  
    $form['repo']['codebase_commit_view_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Commit viewer path'),
      '#description' => t('Link commit to a remote repository viewer. Commit ID will be appended to the URL or swapped for <em>"%s"</em>'),
      '#default_value' => variable_get('codebase_commit_view_url', NULL),
    );
  
    $form['repo']['codebase_branch_view_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Branch viewer path'),
      '#description' => t('Link branch to a remote repository viewer. Branch name will be appended to the URL or swapped for <em>"%s"</em>'),
      '#default_value' => variable_get('codebase_branch_view_url', NULL),
    );
  }
  
  if (module_exists('taskbar')) {
    $form['codebase_taskbar_item'] = array(
      '#type' => 'checkbox',
      '#title' => t('Taskbar integration'),
      '#description' => t('Add a Codebase entry to the Taskbar for privileged users.'),
      '#default_value' => variable_get('codebase_taskbar_item', FALSE),
    );
  }

  return system_settings_form($form);
}
